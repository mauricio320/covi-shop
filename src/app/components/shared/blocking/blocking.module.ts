import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BlockingComponent } from './blocking.component';

@NgModule({
   imports: [
      CommonModule
   ],
   declarations: [
      BlockingComponent
   ],
   exports: [
      BlockingComponent
   ]
})
export class BlockingModule { }
