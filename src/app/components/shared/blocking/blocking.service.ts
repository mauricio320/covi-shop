import { Injectable } from "@angular/core";
import { Subject } from "rxjs";

@Injectable({
  providedIn: "root",
})
export class BlockingService {
  public hidden: Subject<any> = new Subject();
  constructor() {}

  open() {
    this.hidden.next(false);
  }

  close() {
    this.hidden.next(true);
  }

  onObservable() {
    return this.hidden.asObservable();
  }
}
