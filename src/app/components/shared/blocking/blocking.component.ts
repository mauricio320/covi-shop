import { Component, OnInit } from "@angular/core";
import { BlockingService } from "./blocking.service";

@Component({
  selector: "app-blocking",
  templateUrl: "./blocking.component.html",
  styleUrls: ["./blocking.component.scss"],
})
export class BlockingComponent implements OnInit {
  public hidden: boolean;
  constructor(private blockingService: BlockingService) {
    this.hidden = true;
  }

  ngOnInit() {
    this.blockingService.onObservable().subscribe((hidden) => {
      this.hidden = hidden;
    });
  }
}
