import { Injectable } from "@angular/core";
import { Subject } from "rxjs";

@Injectable({
  providedIn: "root",
})
export class IsLoginService {
	private Slogin: Subject<any> = new Subject();
	
  constructor() {}

  get isLogged() {
    const tipo = Number(atob(localStorage.getItem(btoa("tipo"))));
    const id = atob(localStorage.getItem(btoa("id")));
    const loggin = tipo ? true : false;
    return { tipo, id, loggin };
  }

  changeLogin() {
    this.Slogin.next("change");
  }

  SubjectLogin() {
    return this.Slogin.asObservable();
  }
}
