/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { IsLoginService } from './is-login.service';

describe('Service: IsLogin', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [IsLoginService]
    });
  });

  it('should ...', inject([IsLoginService], (service: IsLoginService) => {
    expect(service).toBeTruthy();
  }));
});
