import { Pipe, PipeTransform } from "@angular/core";

@Pipe({
  name: "imgFirebase",
})
export class ImgFirebasePipe implements PipeTransform {
  transform(value: any, args?: any): any {
    return `https://firebasestorage.googleapis.com/v0/b/covi-store.appspot.com/o/${value}?alt=media&token=eaaef6d7-ec55-4915-aafc-1941dfe62e98`;
  }
}
