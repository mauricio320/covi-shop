import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { ImgFirebasePipe } from "./img-firebase.pipe";

@NgModule({
  imports: [CommonModule],
  declarations: [ImgFirebasePipe],
  exports: [ImgFirebasePipe],
})
export class ImgFirebaseModule {}
