import { Injectable } from "@angular/core";
import { AngularFirestore } from "@angular/fire/firestore";

@Injectable({
  providedIn: "root",
})
export class StoreDetailService {
  constructor(private firestore: AngularFirestore) {}

  getDetailEmpresa(empresaId: string) {
    return this.firestore.collection("empresas").doc(empresaId).valueChanges();
  }

  getProductosEmpresa(empresaId: string) {
    return this.firestore
      .collection("productos", (ref) =>
        ref.where("empresa_id", "==", empresaId)
      )
      .snapshotChanges();
  }
}
