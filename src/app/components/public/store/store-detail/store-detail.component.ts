import { Component, OnInit } from "@angular/core";
import { StoreDetailService } from "./store-detail.service";
import { ActivatedRoute } from "@angular/router";

@Component({
  selector: "app-store-detail",
  templateUrl: "./store-detail.component.html",
  styleUrls: ["./store-detail.component.scss"],
})
export class StoreDetailComponent implements OnInit {
  public empresaDetail: any;
  public productosEmpresa: any[];

  constructor(
    private storeDetailService: StoreDetailService,
    private rutaActiva: ActivatedRoute
  ) {
    this.empresaDetail = {};
    this.productosEmpresa = [];
  }

  ngOnInit() {
    this.OnGetEmpresa();
  }

  OnGetEmpresa() {
    const id = this.rutaActiva.snapshot.params.empresaId;
    this.storeDetailService.getDetailEmpresa(id).subscribe((res: any) => {
      this.empresaDetail = res;
    });
    this.storeDetailService.getProductosEmpresa(id).subscribe((res: any) => {
      this.productosEmpresa = res.map((e: any) => {
        return {
          id: e.payload.doc.id,
          ...e.payload.doc.data(),
        };
      });
    });
  }
}
