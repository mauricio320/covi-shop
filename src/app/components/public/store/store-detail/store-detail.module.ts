import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { StoreDetailComponent } from "./store-detail.component";
import { Routes, RouterModule } from "@angular/router";
import { ProductsListModule } from "../../products/products-list/products-list.module";
import { ImgFirebaseModule } from "app/components/shared/img-firebase-pipe/img-firebase.module";

const routes: Routes = [
  { path: ":empresaId/:nameEmpresa", component: StoreDetailComponent },
];

@NgModule({
  imports: [ProductsListModule, CommonModule, RouterModule.forChild(routes), ImgFirebaseModule],
  declarations: [StoreDetailComponent],
})
export class StoreDetailModule {}
