import { Injectable } from "@angular/core";
import { AngularFirestore } from "@angular/fire/firestore";

@Injectable({
  providedIn: "root",
})
export class StoreListService {
  constructor(private firestore: AngularFirestore) {}

  getStores() {
    return this.firestore.collection('empresas').snapshotChanges();
	}
	
	uploadFile() {}
}
 