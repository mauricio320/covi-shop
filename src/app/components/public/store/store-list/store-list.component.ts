import { Component, OnInit } from "@angular/core";
import { StoreListService } from "./store-list.service";
import { Router } from "@angular/router";

@Component({
  selector: "app-store-list",
  templateUrl: "./store-list.component.html",
  styleUrls: ["./store-list.component.scss"],
})
export class StoreListComponent implements OnInit {
  public empresas: any[];

  constructor(
    private storeListService: StoreListService,
    private router: Router
  ) {
    this.empresas = [];
  }

  ngOnInit() {
    this.getStores();
  }

  getStores() {
    this.storeListService.getStores().subscribe((stores) => {
      this.empresas = stores.map((e: any) => {
        return {
          id: e.payload.doc.id,
          ...e.payload.doc.data(),
        };
      });
    });
  }

  navigateDetail(empresa: any) {
    const nombre = empresa.nombre
      .trim()
      .split(" ")
      .join("-")
      .toLocaleLowerCase();
    this.router.navigate([`/store/detail/${empresa.id}/${nombre}`]);
  }
}
