import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { StoreListComponent } from "./store-list.component";
import { Routes, RouterModule } from "@angular/router";
import { ImgFirebaseModule } from "app/components/shared/img-firebase-pipe/img-firebase.module";

const routes: Routes = [{ path: "", component: StoreListComponent }];

@NgModule({
  imports: [CommonModule, RouterModule.forChild(routes), ImgFirebaseModule],
  declarations: [StoreListComponent],
})
export class StoreListModule {}
