/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { StoreListService } from './store-list.service';

describe('Service: StoreList', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [StoreListService]
    });
  });

  it('should ...', inject([StoreListService], (service: StoreListService) => {
    expect(service).toBeTruthy();
  }));
});
