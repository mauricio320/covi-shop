import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { Routes, RouterModule } from "@angular/router";
const store: Routes = [
  {
    path: "list",
    loadChildren: () =>
      import("./store-list/store-list.module").then((m) => m.StoreListModule),
  },
  {
    path: "detail",
    loadChildren: () =>
      import("./store-detail/store-detail.module").then(
        (m) => m.StoreDetailModule
      ),
  },
];

@NgModule({
  imports: [CommonModule, RouterModule.forChild(store)],
  declarations: [],
})
export class StoreModule {}
