import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { IsLoginService } from "app/components/shared/is-loggin/is-login.service";

@Component({
  selector: "app-auth-notLogin",
  templateUrl: "./auth-notLogin.component.html",
  styleUrls: ["./auth-notLogin.component.scss"],
})
export class AuthNotLoginComponent implements OnInit {
  constructor(private router: Router, public isLoginService: IsLoginService) {}

  ngOnInit() {
    localStorage.clear();
		this.isLoginService.changeLogin();
    this.router.navigate(["/store/list"]);
  }
}
