import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { AuthNotLoginComponent } from "./auth-notLogin.component";
import { Routes, RouterModule } from "@angular/router";
const routes: Routes = [{ path: "", component: AuthNotLoginComponent }];
@NgModule({
  imports: [CommonModule, RouterModule.forChild(routes)],
  declarations: [AuthNotLoginComponent],
})
export class AuthNotLoginModule {}
