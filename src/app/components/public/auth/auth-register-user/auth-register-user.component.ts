import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { MessageService } from "primeng/api";
import { BlockingService } from "app/components/shared/blocking/blocking.service";
import { AuthRegisterStoreService } from '../auth-register-store/auth-register-store.service';


@Component({
  selector: 'app-auth-register-user',
  templateUrl: './auth-register-user.component.html',
	styleUrls: ['./auth-register-user.component.scss'],
	providers: [MessageService]
})
export class AuthRegisterUserComponent implements OnInit {

  public categorias: any[];
  public modules = {
    toolbar: [
      [{ header: [1, 2, false] }],
      ["bold", "italic", "underline"],
      ["image"],
    ],
  };

  public auth: any = {};
  public empresa: any = {};
  public inputType: string = "password";
  public file: any;
  public imgPerfil: any =
    "https://www.gravatar.com/avatar/6b472fdba006749bc3e56d90f2e2d81c?s=150&r=g&d=http%3A%2F%2Fbuscatucarga.com%2Fwp-content%2Fthemes%2Fbuscatucarga%2Fimg%2Favatar-blanco.png";

  constructor(
    private authRegisterStoreService: AuthRegisterStoreService,
    private blockingService: BlockingService,
    private messageService: MessageService,
    public router: Router
  ) {
    this.categorias = [];
  }

  ngOnInit() {
  
  }

  onsaveData() {}


  viewImgTemp(file: any) {
    this.file = file;
    var _file: File = file.target.files[0];
    var myReader: FileReader = new FileReader();

    myReader.onloadend = (e) => {
      this.imgPerfil = myReader.result;
    };
    myReader.readAsDataURL(_file);
  }



  async saveData() {
    // valida el correo
    this.blockingService.open();
    this.authRegisterStoreService
      .verificarCorreo(this.empresa.email)
      .subscribe(async (res: any[]) => {
        // si la respuesta es mayor a 0, significa que el correo esta en uso
        if (res.length > 0) {
          this.validUserEmail();
          this.blockingService.close();
        } else {
          // valida si los correo son iguales
          if (this.compareEmail()) {
          
            // crea la empresa
            this.authRegisterStoreService
              .saveUsuario(this.empresa)
              .then((res) => {
                this.auth["usuario"] = res.id;
                this.authRegisterStoreService
                  .saveUser(this.auth)
                  .then((res) => {
                    // redirecciona al login
                    this.router.navigate(["/auth/login"]);
                  });
              });

            return this.blockingService.close();
					}
					return this.blockingService.close();
        }
      });
  }

  viewPassword() {
    if (this.inputType === "text") {
      this.inputType = "password";
    } else {
      this.inputType = "text";
    }
  }

  compareEmail() {
    if (this.empresa.email != this.auth.email) {
      this.messageService.add({
        severity: "warn",
        summary: "Error",
        detail: "Los correo electronicos deben ser iguales",
      });
      return false;
    }

    return true;
  }

  validUserEmail() {
    this.messageService.add({
      severity: "warn",
      summary: "Error",
      detail: "Lo sentimos este correo esta en uso",
    });
  }

}
