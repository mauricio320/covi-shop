import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { AuthLoginComponent } from "./auth-login.component";
import { Routes, RouterModule } from "@angular/router";
import { FormsModule } from "@angular/forms";

const routes: Routes = [{ path: "", component: AuthLoginComponent }];

@NgModule({
  imports: [CommonModule, RouterModule.forChild(routes), FormsModule],
  declarations: [AuthLoginComponent],
})
export class AuthLoginModule {}
