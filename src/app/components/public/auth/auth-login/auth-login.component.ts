import { Component, OnInit } from "@angular/core";
import { AuthLoginService } from "./auth-login.service";
import { BlockingService } from "app/components/shared/blocking/blocking.service";
import { Router } from "@angular/router";
import { IsLoginService } from "app/components/shared/is-loggin/is-login.service";

@Component({
  selector: "app-auth-login",
  templateUrl: "./auth-login.component.html",
  styleUrls: ["./auth-login.component.scss"],
})
export class AuthLoginComponent implements OnInit {
  public auth: { email?: any; password?: any };
  public credentials: boolean = true;

  constructor(
    private authLoginService: AuthLoginService,
    private blockingService: BlockingService,
    public isLoginService: IsLoginService,
    private router: Router
  ) {
    this.auth = {};
  }

  ngOnInit() {}

  login() {
    this.blockingService.open();
    this.authLoginService
      .login(this.auth.email, this.auth.password)
      .subscribe((res: any[]) => {
        const data = res.map((e: any) => {
          return {
            id: e.payload.doc.id,
            ...e.payload.doc.data(),
          };
        });
        if (data.length > 0) {
          this.credentials = true;
          this.onLogin(data[0]);
        } else {
          this.credentials = false;
        }
        this.blockingService.close();
      });
  }

  /**
   * Tipo 1 empresa
   * Tipo 2 usuario
   */
  onLogin(user: any) {
    localStorage.clear();
    if (user.empresa) {
      localStorage.setItem(btoa("tipo"), btoa("1"));
      localStorage.setItem(btoa("id"), btoa(user.empresa));
    }
    if (user.usuario) {
      localStorage.setItem(btoa("tipo"), btoa("2"));
      localStorage.setItem(btoa("id"), btoa(user.usuario));
    }

    localStorage.setItem(btoa("colection"), btoa(user.id));
    this.isLoginService.changeLogin();
    this.router.navigate(["/store/list"]);
  }
}
