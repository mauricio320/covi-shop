import { Injectable } from "@angular/core";
import { AngularFirestore } from "@angular/fire/firestore";
import * as md5 from "md5";
import { Subject } from "rxjs";
// var md5 = require("md5");

@Injectable({
  providedIn: "root",
})
export class AuthLoginService {


  constructor(private firestore: AngularFirestore) {}

  login(email: string, password: string) {
    return this.firestore
      .collection("auth", (ref) =>
        ref.where("email", "==", email).where("password", "==", md5(password))
      )
      .snapshotChanges();
  }


}
