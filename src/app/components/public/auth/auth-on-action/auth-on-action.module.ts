import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { AuthOnActionComponent } from "./auth-on-action.component";
import { Routes, RouterModule } from "@angular/router";

const routes: Routes = [{ path: "", component: AuthOnActionComponent }];

@NgModule({
  imports: [CommonModule, RouterModule.forChild(routes)],
  declarations: [AuthOnActionComponent],
})
export class AuthOnActionModule {}
