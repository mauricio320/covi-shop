import { Component, OnInit } from "@angular/core";
import { AuthRegisterStoreService } from "./auth-register-store.service";
import { Router } from "@angular/router";
import { MessageService } from "primeng/api";
import { BlockingService } from "app/components/shared/blocking/blocking.service";

@Component({
  selector: "app-auth-register-store",
  templateUrl: "./auth-register-store.component.html",
  styleUrls: ["./auth-register-store.component.scss"],
  providers: [MessageService],
})
export class AuthRegisterStoreComponent implements OnInit {
  public categorias: any[];
  public modules = {
    toolbar: [
      [{ header: [1, 2, false] }],
      ["bold", "italic", "underline"],
      ["image"],
    ],
  };

  public auth: any = {};
  public empresa: any = {};
  public inputType: string = "password";
  public file: any;
  public imgPerfil: any =
    "https://www.gravatar.com/avatar/6b472fdba006749bc3e56d90f2e2d81c?s=150&r=g&d=http%3A%2F%2Fbuscatucarga.com%2Fwp-content%2Fthemes%2Fbuscatucarga%2Fimg%2Favatar-blanco.png";

  constructor(
    private authRegisterStoreService: AuthRegisterStoreService,
    private blockingService: BlockingService,
    private messageService: MessageService,
    public router: Router
  ) {
    this.categorias = [];
  }

  ngOnInit() {
    this.getCategorias();
  }

  onsaveData() {}

  getCategorias() {
    this.authRegisterStoreService.getCategorias.subscribe(
      (categorias: any[]) => {
        this.categorias = categorias.map((item) => {
          return { label: item.nombre, value: item.nombre };
        });
      }
    );
  }

  viewImgTemp(file: any) {
    this.file = file;
    var _file: File = file.target.files[0];
    var myReader: FileReader = new FileReader();

    myReader.onloadend = (e) => {
      this.imgPerfil = myReader.result;
    };
    myReader.readAsDataURL(_file);
  }

  async saveImgAssets(file: any) {
    await this.authRegisterStoreService.tareaCloudStorage(file).then((res) => {
      this.empresa["img"] = res.metadata.fullPath;
    });
  }

  async saveData() {
    // valida el correo
    this.blockingService.open();
    this.authRegisterStoreService
      .verificarCorreo(this.empresa.email)
      .subscribe(async (res: any[]) => {
        // si la respuesta es mayor a 0, significa que el correo esta en uso
        if (res.length > 0) {
          this.validUserEmail();
          this.blockingService.close();
        } else {
          // valida si los correo son iguales
          if (this.compareEmail()) {
           
            // guarda la img
            await this.saveImgAssets(this.file);
            // crea la empresa
            this.authRegisterStoreService
              .saveEmpresa(this.empresa)
              .then((res) => {
                this.auth["empresa"] = res.id;
                this.authRegisterStoreService
                  .saveUser(this.auth)
                  .then((res) => {
                    // redirecciona al login
                    this.router.navigate(["/auth/login"]);
                  });
              });

            return this.blockingService.close();
					}
					return this.blockingService.close();
        }
      });
  }

  viewPassword() {
    if (this.inputType === "text") {
      this.inputType = "password";
    } else {
      this.inputType = "text";
    }
  }

  compareEmail() {
    if (this.empresa.email != this.auth.email) {
      this.messageService.add({
        severity: "warn",
        summary: "Error",
        detail: "Los correo electronicos deben ser iguales",
      });
      return false;
    }

    return true;
  }

  validUserEmail() {
    this.messageService.add({
      severity: "warn",
      summary: "Error",
      detail: "Lo sentimos este correo esta en uso",
    });
  }
}
