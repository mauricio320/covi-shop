import { Injectable } from "@angular/core";
import { AngularFirestore } from "@angular/fire/firestore";
import { AngularFireStorage } from "@angular/fire/storage";
import * as uuid from "uuid";
import * as md5 from "md5";

@Injectable({
  providedIn: "root",
})
export class AuthRegisterStoreService {
  constructor(
    private firestore: AngularFirestore,
    private angularFireStorage: AngularFireStorage
  ) {}

  get getCategorias() {
    return this.firestore.collection("categorias").valueChanges();
  }

  //Tarea para subir archivo
  public tareaCloudStorage(file: any) {
    const myId = uuid.v4();
    const ext = file.target.files[0].name.split(".").pop();
    const name = `${myId}.${ext}`;

    const datosFormulario = new FormData();
    datosFormulario.append("archivo", file.target.files[0], name);
    let archivo = datosFormulario.get("archivo");
    return this.angularFireStorage.upload(name, archivo);
  }

  /**
   *
   */
  public deleteFile(ref: any) {
    return this.angularFireStorage.ref(ref).delete();
  }

  public referenciaCloudStorage(nombreArchivo: string) {
    return this.angularFireStorage.ref(nombreArchivo);
  }

  /**
   *
   * @param data
   */
  public saveEmpresa(data: any) {
    return this.firestore.collection("empresas").add(data);
  }

  /**
   *
   * @param data
   */
  public saveUsuario(data: any) {
    return this.firestore.collection("usuarios").add(data);
  }

  public saveUser(data: any) {
    const _data = Object.assign({}, data);
    _data.password = md5(data.password);
    return this.firestore.collection("auth").add(_data);
  }

  public verificarCorreo(email: string) {
    return this.firestore
      .collection("auth", (ref) => ref.where("email", "==", email))
      .valueChanges();
  }

  public infoEmpresa() {
    let id = localStorage.getItem(btoa("id"));
    id = atob(id);
    return this.firestore.collection("empresas").doc(id).valueChanges();
  }

  public infoUsuario() {
    let id = localStorage.getItem(btoa("id"));
    id = atob(id);
    return this.firestore.collection("usuarios").doc(id).valueChanges();
  }

  public updateEmpresa(data: any) {
    let id = localStorage.getItem(btoa("id"));
    id = atob(id);
    return this.firestore.collection("empresas").doc(id).update(data);
	}
	
	public updateUsuarios(data: any) {
    let id = localStorage.getItem(btoa("id"));
    id = atob(id);
    return this.firestore.collection("usuarios").doc(id).update(data);
  }

  public updateAuht(data: any) {
    let id = localStorage.getItem(btoa("colection"));
    id = atob(id);
    return this.firestore.collection("auth").doc(id).update(data);
  }
}
