import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { AuthRegisterStoreComponent } from "./auth-register-store.component";
import { Routes, RouterModule } from "@angular/router";
import { QuillModule } from "ngx-quill";
import { FormsModule } from "@angular/forms";
import { DropdownModule } from "primeng/dropdown";
import { ToastModule } from "primeng/toast";

const routes: Routes = [{ path: "", component: AuthRegisterStoreComponent }];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    QuillModule.forRoot(),
    FormsModule,
		DropdownModule,
		ToastModule
  ],
  declarations: [AuthRegisterStoreComponent],
})
export class AuthRegisterStoreModule {}
