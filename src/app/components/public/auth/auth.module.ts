import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { Routes, RouterModule } from "@angular/router";
const store: Routes = [
  {
    path: "login",
    loadChildren: () =>
      import("./auth-login/auth-login.module").then((m) => m.AuthLoginModule),
  },
  {
    path: "action",
    loadChildren: () =>
      import("./auth-on-action/auth-on-action.module").then(
        (m) => m.AuthOnActionModule
      ),
  },
  {
    path: "action/store",
    loadChildren: () =>
      import("./auth-register-store/auth-register-store.module").then(
        (m) => m.AuthRegisterStoreModule
      ),
  },
  {
    path: "action/user",
    loadChildren: () =>
      import("./auth-register-user/auth-register-user.module").then(
        (m) => m.AuthRegisterUserModule
      ),
  },
  {
    path: "isLogin",
    loadChildren: () =>
      import("./auth-notLogin/auth-notLogin.module").then(
        (m) => m.AuthNotLoginModule
      ),
  },
];

@NgModule({
  imports: [CommonModule, RouterModule.forChild(store)],
  declarations: [],
})
export class AuthModule {}
