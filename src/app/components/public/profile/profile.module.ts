import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { Routes, RouterModule } from "@angular/router";

const profile: Routes = [
  {
    path: "user",
    loadChildren: () =>
      import("./profile-user/profile-user.module").then(
        (m) => m.ProfileUserModule
      ),
  },
  {
    path: "store",
    loadChildren: () =>
      import("./profile-store/profile-store.module").then(
        (m) => m.ProfileStoreModule
      ),
  },
];

@NgModule({
  imports: [CommonModule, RouterModule.forChild(profile)],
  declarations: [],
})
export class ProfileModule {}
