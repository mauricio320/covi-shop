import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { ProfileStoreComponent } from "./profile-store.component";
import { Routes, RouterModule } from "@angular/router";
import { QuillModule } from "ngx-quill";
import { FormsModule } from "@angular/forms";
import { DropdownModule } from "primeng/dropdown";
import { ToastModule } from "primeng/toast";
import { ImgFirebaseModule } from "app/components/shared/img-firebase-pipe/img-firebase.module";

const routes: Routes = [{ path: "", component: ProfileStoreComponent }];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    QuillModule.forRoot(),
    FormsModule,
    DropdownModule,
		ToastModule,
		ImgFirebaseModule,
  ],
  declarations: [ProfileStoreComponent],
})
export class ProfileStoreModule {}
