import { Component, OnInit } from "@angular/core";

import { Router } from "@angular/router";
import { MessageService } from "primeng/api";
import { BlockingService } from "app/components/shared/blocking/blocking.service";
import { AuthRegisterStoreService } from "../../auth/auth-register-store/auth-register-store.service";

@Component({
  selector: "app-profile-store",
  templateUrl: "./profile-store.component.html",
  styleUrls: ["./profile-store.component.scss"],
  providers: [MessageService],
})
export class ProfileStoreComponent implements OnInit {
  public categorias: any[];
  public modules = {
    toolbar: [
      [{ header: [1, 2, false] }],
      ["bold", "italic", "underline"],
      ["image"],
    ],
  };

  public auth: any = {};
  public empresa: any = {};
  public inputType: string = "password";
  constructor(
    private authRegisterStoreService: AuthRegisterStoreService,
    private blockingService: BlockingService,
    private messageService: MessageService,
    public router: Router
  ) {
    this.categorias = [];
  }

  ngOnInit() {
    this.getCategorias();
    this.getUser();
  }

  getUser() {
    this.authRegisterStoreService.infoEmpresa().subscribe((res) => {
      this.empresa = res;
    });
  }

  getCategorias() {
    this.blockingService.open();
    this.authRegisterStoreService.getCategorias.subscribe(
      (categorias: any[]) => {
        this.categorias = categorias.map((item) => {
          return { label: item.nombre, value: item.nombre };
        });
        this.blockingService.close();
      }
    );
  }

  async saveImgAssets(file: any) {
    this.authRegisterStoreService.deleteFile(this.empresa.img);
    await this.authRegisterStoreService.tareaCloudStorage(file).then((res) => {
      this.empresa["img"] = res.metadata.fullPath;
    });
  }

  async updateImg(file: any) {
    this.blockingService.open();
    await this.saveImgAssets(file);
    this.authRegisterStoreService.updateEmpresa(this.empresa).then((res) => {
      this.blockingService.close();
    });
  }

  async updateData() {
    // valida el correo
    this.blockingService.open();
    this.authRegisterStoreService
      .verificarCorreo(this.empresa.email)
      .subscribe(async (res: any[]) => {
        // si la respuesta es mayor a 0, significa que el correo esta en uso
        if (res.length > 0) {
          let id = localStorage.getItem(btoa("id"));
          id = atob(id);
          if (res[0].empresa === id) {
            this.udpdates();
          } else {
            this.validUserEmail();
            this.blockingService.close();
          }
        } else {
          // valida si los correo son iguales
          this.udpdates();
        }
      });
  }

  udpdates() {
    this.authRegisterStoreService.updateEmpresa(this.empresa).then((res) => {
      this.updatedShop();
      this.blockingService.close();
    });

    this.authRegisterStoreService
      .updateAuht({ email: this.empresa.email })
      .then((res) => {
       
      });
  }

  viewPassword() {
    if (this.inputType === "text") {
      this.inputType = "password";
    } else {
      this.inputType = "text";
    }
  }

  updatedShop() {
    if (this.empresa.email != this.auth.email) {
      this.messageService.add({
        severity: "success",
        summary: "Confirmación",
        detail: "Datos actualizados correctamente",
      });
      return false;
    }

    return true;
  }

  validUserEmail() {
    this.messageService.add({
      severity: "warn",
      summary: "Error",
      detail: "Lo sentimos este correo esta en uso",
    });
  }
}
