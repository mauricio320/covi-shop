import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { ProfileUserComponent } from "./profile-user.component";
import { Routes, RouterModule } from "@angular/router";
import { QuillModule } from "ngx-quill";
import { FormsModule } from "@angular/forms";
import { DropdownModule } from "primeng/dropdown";
import { ToastModule } from "primeng/toast";

const routes: Routes = [{ path: "", component: ProfileUserComponent }];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    QuillModule.forRoot(),
    FormsModule,
    DropdownModule,
    ToastModule,
  ],
  declarations: [ProfileUserComponent],
})
export class ProfileUserModule {}
