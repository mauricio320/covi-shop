import { Component, OnInit } from "@angular/core";
import { MessageService } from "primeng/api";
import { BlockingService } from "app/components/shared/blocking/blocking.service";
import { Router } from "@angular/router";
import { AuthRegisterStoreService } from "../../auth/auth-register-store/auth-register-store.service";

@Component({
  selector: "app-profile-user",
  templateUrl: "./profile-user.component.html",
  styleUrls: ["./profile-user.component.scss"],
  providers: [MessageService],
})
export class ProfileUserComponent implements OnInit {
  public categorias: any[];
  public auth: any = {};
  public empresa: any = {};
  public inputType: string = "password";
  public file: any;

  constructor(
    private authRegisterStoreService: AuthRegisterStoreService,
    private blockingService: BlockingService,
    private messageService: MessageService,
    public router: Router
  ) {
    this.categorias = [];
  }

  ngOnInit() {
    this.getUser();
  }

  getUser() {
    this.blockingService.open();
    this.authRegisterStoreService.infoUsuario().subscribe((res) => {
      this.blockingService.close();
      this.empresa = res;
    });
  }

  async saveData() {
    // valida el correo
    this.blockingService.open();
    this.authRegisterStoreService
      .verificarCorreo(this.empresa.email)
      .subscribe(async (res: any[]) => {
        // si la respuesta es mayor a 0, significa que el correo esta en uso
        if (res.length > 0) {
          let id = localStorage.getItem(btoa("id"));
          id = atob(id);
          if (res[0].usuario === id) {
            this.updateData();
          } else {
            this.validUserEmail();
            this.blockingService.close();
          }
        } else {
          this.updateData();
        }
      });
  }

  updateData() {
    this.blockingService.open();
    this.authRegisterStoreService.updateUsuarios(this.empresa).then((res) => {
      this.updatedShop();
      this.blockingService.close();
    });

    this.authRegisterStoreService
      .updateAuht({ email: this.empresa.email })
      .then((res) => {});
  }

  viewPassword() {
    if (this.inputType === "text") {
      this.inputType = "password";
    } else {
      this.inputType = "text";
    }
  }

  updatedShop() {
    if (this.empresa.email != this.auth.email) {
      this.messageService.add({
        severity: "success",
        summary: "Confirmación",
        detail: "Datos actualizados correctamente",
      });
    }
  }

  validUserEmail() {
    this.messageService.add({
      severity: "warn",
      summary: "Error",
      detail: "Lo sentimos este correo esta en uso",
    });
  }
}
