import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { ChatContactsMobileComponent } from "./chat-contacts-mobile.component";
import { Routes, RouterModule } from "@angular/router";
import { ContactsModule } from "../contacts/contacts.module";
import { MessageModule } from "../message/message.module";

const routes: Routes = [{ path: "", component: ChatContactsMobileComponent }];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    ContactsModule,
    MessageModule,
  ],
  declarations: [ChatContactsMobileComponent],
})
export class ChatContactsMobileModule {}
