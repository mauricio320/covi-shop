import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";

@Component({
  selector: "app-message",
  templateUrl: "./message.component.html",
  styleUrls: ["./message.component.scss"],
})
export class MessageComponent implements OnInit {
  @Input() header: { title?: string; subTitle?: string } = {};
  @Input() messages: any[] = [];
  @Output() message: EventEmitter<any> = new EventEmitter();
  public string: string;
  constructor() {}

  ngOnInit() {}

  sendMessage() {
    this.message.emit(this.string);
    this.string = null;
  }
}
