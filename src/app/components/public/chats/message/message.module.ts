import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { MessageComponent } from "./message.component";
import { FormsModule } from "@angular/forms";
import { messagePipe } from "./message.pipe";

@NgModule({
  imports: [CommonModule, FormsModule],
  declarations: [MessageComponent, messagePipe],
  exports: [MessageComponent],
})
export class MessageModule {}
