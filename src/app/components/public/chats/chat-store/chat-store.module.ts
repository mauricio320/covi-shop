import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { ChatStoreComponent } from "./chat-store.component";
import { Routes, RouterModule } from "@angular/router";
import { ContactsModule } from "../contacts/contacts.module";
import { MessageModule } from "../message/message.module";

const routes: Routes = [{ path: "", component: ChatStoreComponent }];

@NgModule({
  imports: [CommonModule, RouterModule.forChild(routes), ContactsModule, MessageModule],
  declarations: [ChatStoreComponent],
})
export class ChatStoreModule {}
