import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { ChatProductService } from "../chat-product/chat-product.service";
import { BlockingService } from "app/components/shared/blocking/blocking.service";
import * as moment from "moment";

@Component({
  selector: "app-chat-store",
  templateUrl: "./chat-store.component.html",
  styleUrls: ["./chat-store.component.scss"],
})
export class ChatStoreComponent implements OnInit {
  public chats: any[];
  public header: any;
  public messages: any[];
  constructor(
    private router: Router,
    private chatProductService: ChatProductService,
    private blockingService: BlockingService
  ) {
    this.chats = [];
    this.header = {};
    this.messages = [];
  }

  ngOnInit() {
    if ($(window).width() > 991) {
      this.loadChats();
    }
  }

  isMobileChat() {
    if ($(window).width() > 991) {
      return true;
    }
    this.router.navigate(["/chat/store/mobile"]);
    return false;
  }

  loadChats() {
    this.blockingService.open();
    this.chatProductService.getChatsStore().subscribe((res: any) => {
      for (const iterator of res) {
        this.chatProductService
          .getDocProductoId(iterator.producto_id)
          .subscribe((rs: any) => {
            iterator["Producto"] = rs;
            iterator["subTitle"] = `${rs.nombre}`;
          });

        this.chatProductService
          .getDocUsuarioId(iterator.id)
          .subscribe((rs: any) => {
            iterator["title"] = `${rs.nombres} ${rs.apellidos}`;
          });
      }

      this.chats = res;
      this.blockingService.close();
    });
  }

  chatSelecionado(chat: any) {
    moment.locale("es");
    const date = moment(chat.date).format("MMMM Do YYYY, h:mm a");
    this.header = { ...chat };
    this.header.subTitle = `${chat.subTitle} - ${date}`;
    this.blockingService.open();
    this.chatProductService
      .getMessagesIdColetion(chat.idCollection)
      .subscribe((res) => {
        this.messages = res;
        this.blockingService.close();
      });
  }

  createMessages(message) {
    if (message) {
      const productoId = this.header.producto_id;
      const empresaId = this.header.empresa_id;
      this.blockingService.open();
      this.chatProductService
        .addMessageStore(
          productoId,
          empresaId,
          message,
          this.header.idCollection
        )
        .then((res) => {
          this.blockingService.close();
        });
    }
  }
}
