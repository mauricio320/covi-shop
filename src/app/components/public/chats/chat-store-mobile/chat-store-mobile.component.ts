import { Component, OnInit } from "@angular/core";
import { ChatProductService } from "../chat-product/chat-product.service";
import * as moment from "moment";
import { BlockingService } from "app/components/shared/blocking/blocking.service";
import { Router } from "@angular/router";

@Component({
  selector: "app-chat-store-mobile",
  templateUrl: "./chat-store-mobile.component.html",
  styleUrls: ["./chat-store-mobile.component.scss"],
})
export class ChatStoreMobileComponent implements OnInit {
  public chats: any[];
  public header: any;
  public messages: any[];
  public step: number = 1;

  constructor(
    private router: Router,
    private chatProductService: ChatProductService,
    private blockingService: BlockingService
  ) {
    this.chats = [];
    this.header = {};
    this.messages = [];
  }

  ngOnInit() {
    this.loadChats();
  }

  loadChats() {
    this.blockingService.open();
    this.chatProductService.getChatsStore().subscribe((res: any) => {
      for (const iterator of res) {
        this.chatProductService
          .getDocProductoId(iterator.producto_id)
          .subscribe((rs: any) => {
            iterator["Producto"] = rs;
            iterator["subTitle"] = `${rs.nombre}`;
          });

        this.chatProductService
          .getDocUsuarioId(iterator.id)
          .subscribe((rs: any) => {
            iterator["title"] = `${rs.nombres} ${rs.apellidos}`;
          });
      }

      this.chats = res;
      this.blockingService.close();
    });
  }

  chatSelecionado(chat: any) {
    moment.locale("es");
    const date = moment(chat.date).format("MMMM Do YYYY, h:mm a");
    this.header = { ...chat };
    this.header.subTitle = `${chat.subTitle} - ${date}`;
    this.step = 2;

    this.blockingService.open();
    this.chatProductService
      .getMessagesIdColetion(chat.idCollection)
      .subscribe((res) => {
        this.messages = res;
        this.blockingService.close();
      });
  }

  createMessages(message) {
    if (message) {
      const productoId = this.header.producto_id;
      const empresaId = this.header.empresa_id;
      this.blockingService.open();
      this.chatProductService
        .addMessageStore(
          productoId,
          empresaId,
          message,
          this.header.idCollection
        )
        .then((res) => {
          this.blockingService.close();
        });
    }
  }
}
