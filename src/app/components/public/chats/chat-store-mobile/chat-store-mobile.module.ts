import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { ChatStoreMobileComponent } from "./chat-store-mobile.component";
import { Routes, RouterModule } from "@angular/router";
import { ContactsModule } from "../contacts/contacts.module";
import { MessageModule } from "../message/message.module";

const routes: Routes = [{ path: "", component: ChatStoreMobileComponent }];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    ContactsModule,
    MessageModule,
  ],
  declarations: [ChatStoreMobileComponent],
})
export class ChatStoreMobileModule {}
