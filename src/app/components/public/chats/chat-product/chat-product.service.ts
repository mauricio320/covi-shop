import { Injectable } from "@angular/core";
import { AngularFirestore } from "@angular/fire/firestore";
import { map, switchMap, mergeMap } from "rxjs/operators";
import { from } from "rxjs";

@Injectable({
  providedIn: "root",
})
export class ChatProductService {
  constructor(private angularFirestore: AngularFirestore) {}

  getProducto(productoId: string) {
    return this.angularFirestore
      .collection("productos")
      .doc(productoId)
      .valueChanges();
  }

  getChat(productoId: string, empresaId: string) {
    let id = localStorage.getItem(btoa("id"));
    id = atob(id);

    let tipo = localStorage.getItem(btoa("tipo"));
    tipo = atob(tipo);

    return this.angularFirestore
      .collection("chat")
      .doc(`${productoId}-${empresaId}-${id}`)
      .set({
        empresa_id: empresaId,
        producto_id: productoId,
        id,
        tipo,
        date: Date.now(),
      });
  }

  getMessages(productoId: string, empresaId: string) {
    let id = localStorage.getItem(btoa("id"));
    id = atob(id);
    return this.angularFirestore
      .collection("messages", (ref) =>
        ref
          .where("chat_id", "==", `${productoId}-${empresaId}-${id}`)
          .orderBy("date", "asc")
      )
      .valueChanges();
  }

  getMessagesIdColetion(id: string) {
    return this.angularFirestore
      .collection("messages", (ref) =>
        ref.where("chat_id", "==", id).orderBy("date", "asc")
      )
      .valueChanges();
  }

  addMessage(productoId: string, empresaId: string, message: string) {
    let id = localStorage.getItem(btoa("id"));
    id = atob(id);

    let tipo = localStorage.getItem(btoa("tipo"));
    tipo = atob(tipo);

    this.getChat(productoId, empresaId);

    return this.angularFirestore.collection("messages").add({
      empresa_id: empresaId,
      producto_id: productoId,
      chat_id: `${productoId}-${empresaId}-${id}`,
      message,
      id,
      tipo,
      date: Date.now(),
    });
  }

  addMessageStore(productoId: string, empresaId: string, message: string, chatId: any) {
    let id = localStorage.getItem(btoa("id"));
    id = atob(id);

    let tipo = localStorage.getItem(btoa("tipo"));
    tipo = atob(tipo);

    this.updatedChat(chatId);

    return this.angularFirestore.collection("messages").add({
      empresa_id: empresaId,
      producto_id: productoId,
      chat_id: chatId,
      message,
      id,
      tipo,
      date: Date.now(),
    });
  }

  updatedChat(chatId) {
    let id = localStorage.getItem(btoa("id"));
    id = atob(id);

    let tipo = localStorage.getItem(btoa("tipo"));
    tipo = atob(tipo);

    return this.angularFirestore.collection("chat").doc(`${chatId}`).update({
      date: Date.now(),
    });
  }

  getChatsUser() {
    let id = localStorage.getItem(btoa("id"));
    id = atob(id);
    return this.angularFirestore
      .collection("chat", (ref) =>
        ref.where("id", "==", id).orderBy("date", "desc")
      )
      .snapshotChanges()
      .pipe(
        map((e: any) => {
          return e.map((i) => {
            const data = {
              idCollection: i.payload.doc.id,
              ...i.payload.doc.data(),
            };
            return data;
          });
        })
      );
  }

  getChatsStore() {
    let id = localStorage.getItem(btoa("id"));
    id = atob(id);
    return this.angularFirestore
      .collection("chat", (ref) =>
        ref.where("empresa_id", "==", id).orderBy("date", "desc")
      )
      .snapshotChanges()
      .pipe(
        map((e: any) => {
          return e.map((i) => {
            const data = {
              idCollection: i.payload.doc.id,
              ...i.payload.doc.data(),
            };
            return data;
          });
        })
      );
  }

  getDocProductoId(productoId: string) {
    return this.angularFirestore
      .collection("productos")
      .doc(productoId)
      .valueChanges();
  }

  getDocUsuarioId(usuarioId: string) {
    return this.angularFirestore
      .collection("usuarios")
      .doc(usuarioId)
      .valueChanges();
  }
}
