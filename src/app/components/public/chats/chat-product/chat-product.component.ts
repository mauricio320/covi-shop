import { Component, OnInit } from "@angular/core";
import { ChatProductService } from "./chat-product.service";
import { ActivatedRoute } from "@angular/router";

@Component({
  selector: "app-chat-product",
  templateUrl: "./chat-product.component.html",
  styleUrls: ["./chat-product.component.scss"],
})
export class ChatProductComponent implements OnInit {
  public header = { title: "", subtitle: "" };
  public messages = [];
  constructor(
    private chatProductService: ChatProductService,
    private route: ActivatedRoute
  ) {}

  ngOnInit() {
    this.getProducto();
    this.getMessages();
  }

  getProducto() {
    const productoId = this.route.snapshot.params.productoId;
    this.chatProductService.getProducto(productoId).subscribe((res: any) => {
      this.header.title = res.nombre;
      this.header.subtitle = `$${res.precio.toLocaleString()}`;
    });
  }


  getMessages() {
    const productoId = this.route.snapshot.params.productoId;
    const empresaId = this.route.snapshot.params.empresaId;
    this.chatProductService
      .getMessages(productoId, empresaId)
      .subscribe((res) => {
        this.messages = res;
      });
  }

  createMessages(message: string) {
    if (message) {
      const productoId = this.route.snapshot.params.productoId;
      const empresaId = this.route.snapshot.params.empresaId;
      this.chatProductService
        .addMessage(productoId, empresaId, message)
        .then((res) => {
      
        });
    }
  }
}
