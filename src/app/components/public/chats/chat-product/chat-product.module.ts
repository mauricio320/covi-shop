import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { ChatProductComponent } from "./chat-product.component";
import { Routes, RouterModule } from "@angular/router";
import { MessageModule } from "../message/message.module";

const routes: Routes = [{ path: ":productoId/:empresaId", component: ChatProductComponent }];

@NgModule({
  imports: [CommonModule, RouterModule.forChild(routes), MessageModule],
  declarations: [ChatProductComponent],
  exports: [ChatProductComponent],
})
export class ChatProductModule {}
