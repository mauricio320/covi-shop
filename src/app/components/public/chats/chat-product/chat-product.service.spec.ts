/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { ChatProductService } from './chat-product.service';

describe('Service: ChatProduct', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ChatProductService]
    });
  });

  it('should ...', inject([ChatProductService], (service: ChatProductService) => {
    expect(service).toBeTruthy();
  }));
});
