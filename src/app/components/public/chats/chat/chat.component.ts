import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { ChatProductService } from "../chat-product/chat-product.service";
import * as moment from "moment";
import { BlockingService } from "app/components/shared/blocking/blocking.service";

@Component({
  selector: "app-chat",
  templateUrl: "./chat.component.html",
  styleUrls: ["./chat.component.scss"],
})
export class ChatComponent implements OnInit {
  public chats: any[];
  public header: any;
  public messages: any[];
  constructor(
    private router: Router,
    private chatProductService: ChatProductService,
    private blockingService: BlockingService
  ) {
    this.chats = [];
    this.header = {};
    this.messages = [];
  }

  ngOnInit() {
    if ($(window).width() > 991) {
      this.loadChats();
      return true;
    }
  }

  isMobileChat() {
    if ($(window).width() > 991) {
      return true;
    }
    this.router.navigate(["/chat/user/mobile"]);
    return false;
  }

  loadChats() {
    this.blockingService.open();
    this.chatProductService.getChatsUser().subscribe(async (res: any) => {
      for (const iterator of res) {
        this.chatProductService
          .getDocProductoId(iterator.producto_id)
          .subscribe((rs: any) => {
            iterator["Producto"] = rs;
            iterator["title"] = rs.nombre;
            moment.locale("es");
            iterator["subTitle"] = moment(iterator.date).format(
              "MMMM Do YYYY, h:mm a"
            );
          });
      }

      this.chats = res;
      this.blockingService.close();
    });
  }

  chatSelecionado(chat: any) {
    this.header = chat;
    this.blockingService.open();
    this.chatProductService
      .getMessagesIdColetion(chat.idCollection)
      .subscribe((res) => {
        this.messages = res;
        this.blockingService.close();
      });
  }

  createMessages(message) {
    if (message) {
      const productoId = this.header.producto_id;
      const empresaId = this.header.empresa_id;
      this.blockingService.open();
      this.chatProductService
        .addMessage(productoId, empresaId, message)
        .then((res) => {
          this.blockingService.close();
        });
    }
  }
}
