import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { ChatComponent } from "./chat.component";
import { ContactsModule } from "../contacts/contacts.module";
import { MessageModule } from "../message/message.module";

@NgModule({
  imports: [CommonModule, ContactsModule, MessageModule],
  declarations: [ChatComponent],
  exports: [ChatComponent],
})
export class ChatModule {}
