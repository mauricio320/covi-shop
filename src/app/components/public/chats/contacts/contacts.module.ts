import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { ContactsComponent } from "./contacts.component";
import { ScrollPanelModule } from "primeng/scrollpanel";

@NgModule({
  imports: [CommonModule, ScrollPanelModule],
  declarations: [ContactsComponent],
  exports: [ContactsComponent],
})
export class ContactsModule {}
