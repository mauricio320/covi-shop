import {
  Component,
  OnInit,
  Input,
  OnChanges,
  Output,
  EventEmitter,
} from "@angular/core";

@Component({
  selector: "app-contacts",
  templateUrl: "./contacts.component.html",
  styleUrls: ["./contacts.component.scss"],
})
export class ContactsComponent implements OnInit, OnChanges {
  @Input() chats: { title?: string; subTitle?: string }[] = [];
  @Output() chatSelect: EventEmitter<any> = new EventEmitter();

  constructor() {}

  ngOnInit() {}

  ngOnChanges(): void {

  }

  emitChat(chat: any) {
    this.chatSelect.emit(chat);
  }
}
