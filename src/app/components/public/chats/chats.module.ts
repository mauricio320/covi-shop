import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { Routes, RouterModule } from "@angular/router";
const store: Routes = [
  {
    path: "user",
    loadChildren: () =>
      import("./chats-user/chats-user.module").then((m) => m.ChatsUserModule),
  },
  {
    path: "user/mobile",
    loadChildren: () =>
      import("./chat-contacts-mobile/chat-contacts-mobile.module").then(
        (m) => m.ChatContactsMobileModule
      ),
  },
  {
    path: "product",
    loadChildren: () =>
      import("./chat-product/chat-product.module").then(
        (m) => m.ChatProductModule
      ),
  },
  {
    path: "store",
    loadChildren: () =>
      import("./chat-store/chat-store.module").then((m) => m.ChatStoreModule),
  },
  {
    path: "store/mobile",
    loadChildren: () =>
      import("./chat-store-mobile/chat-store-mobile.module").then(
        (m) => m.ChatStoreMobileModule
      ),
  },
];

@NgModule({
  imports: [CommonModule, RouterModule.forChild(store)],
  declarations: [],
})
export class ChatsModule {}
