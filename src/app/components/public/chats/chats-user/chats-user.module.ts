import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { ChatsUserComponent } from "./chats-user.component";
import { Routes, RouterModule } from "@angular/router";
import { ChatModule } from "../chat/chat.module";

const routes: Routes = [{ path: "", component: ChatsUserComponent }];

@NgModule({
  imports: [CommonModule, RouterModule.forChild(routes), ChatModule],
  declarations: [ChatsUserComponent],
})
export class ChatsUserModule {}
