import { Component, OnInit, Input, OnChanges } from "@angular/core";
import { Router } from "@angular/router";

@Component({
  selector: "app-products-list",
  templateUrl: "./products-list.component.html",
  styleUrls: ["./products-list.component.scss"],
})
export class ProductsListComponent implements OnInit {
  @Input() productos: any[];

  constructor(private router: Router) {
    this.productos = [];
  }

  ngOnInit() {}

  redirecDetailProduct(producto: any) {
    const nombre = producto.nombre
      .trim()
      .split(" ")
      .join("-")
      .toLocaleLowerCase();

    this.router.navigate([`products/detail/${producto.id}/${nombre}`]);
  }

  redirecChatProducto(producto: any) {
    this.router.navigate([
      `/chat/product/${producto.id}/${producto.empresa_id}`,
    ]);
  }
}
