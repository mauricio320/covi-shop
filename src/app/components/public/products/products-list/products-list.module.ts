import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProductsListComponent } from './products-list.component';
import { ImgFirebaseModule } from 'app/components/shared/img-firebase-pipe/img-firebase.module';

@NgModule({
  imports: [
		CommonModule,
		ImgFirebaseModule
  ],
	declarations: [ProductsListComponent],
	exports:[ProductsListComponent]
})
export class ProductsListModule { }
