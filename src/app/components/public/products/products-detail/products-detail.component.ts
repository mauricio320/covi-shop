import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { ProductsDetailService } from "./products-detail.service";
import { StoreDetailService } from "../../store/store-detail/store-detail.service";

@Component({
  selector: "app-products-detail",
  templateUrl: "./products-detail.component.html",
  styleUrls: ["./products-detail.component.scss"],
})
export class ProductsDetailComponent implements OnInit {
  public producto: any;
  public empresaDetail: any;

  constructor(
    private rutaActiva: ActivatedRoute,
    private productsDetailService: ProductsDetailService,
    private storeDetailService: StoreDetailService,
    private router: Router
  ) {
    this.producto = {};
    this.empresaDetail = {};
  }

  ngOnInit() {
    this.getProducto();
  }

  getProducto() {
    const id = this.rutaActiva.snapshot.params.productoId;
    this.productsDetailService.getProducto(id).subscribe((res: any) => {
      this.producto = res;
      this.getEmpresa(res.empresa_id);
    });
  }

  getEmpresa(empresaId: any) {
    this.storeDetailService.getDetailEmpresa(empresaId).subscribe((res) => {
      this.empresaDetail = res;
    });
  }

  navigateEmpresa() {
    const nombre = this.empresaDetail.nombre
      .trim()
      .split(" ")
      .join("-")
      .toLocaleLowerCase();
    this.router.navigate([
      `/store/detail/${this.producto.empresa_id}/${nombre}`,
    ]);
  }
}
