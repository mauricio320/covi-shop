import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { ProductsDetailComponent } from "./products-detail.component";
import { Routes, RouterModule } from "@angular/router";
import { ImgFirebaseModule } from "app/components/shared/img-firebase-pipe/img-firebase.module";

const routes: Routes = [
  { path: ":productoId/:nameProducto", component: ProductsDetailComponent },
];

@NgModule({
  imports: [CommonModule, RouterModule.forChild(routes), ImgFirebaseModule],
  declarations: [ProductsDetailComponent],
})
export class ProductsDetailModule {}
