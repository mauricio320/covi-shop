import { Injectable } from "@angular/core";
import { AngularFirestore } from "@angular/fire/firestore";

@Injectable({
  providedIn: "root",
})
export class ProductsDetailService {
  constructor(private firestore: AngularFirestore) {}

  getProducto(id: any) {
    return this.firestore.collection("productos").doc(id).valueChanges();
  }
}
