import { Component, OnInit } from "@angular/core";
import { ProductsListGenericService } from "./products-list-generic.service";

@Component({
  selector: "app-products-list-generic",
  templateUrl: "./products-list-generic.component.html",
  styleUrls: ["./products-list-generic.component.scss"],
})
export class ProductsListGenericComponent implements OnInit {
  public productos: any[];

  constructor(private productsListGenericService: ProductsListGenericService) {
    this.productos = [];
  }

  ngOnInit() {
    this.getProductos();
  }

  getProductos() {
    this.productsListGenericService.getProductos().subscribe((res) => {
      this.productos = res.map((e: any) => {
        return {
          id: e.payload.doc.id,
          ...e.payload.doc.data(),
        };
      });
    });
  }
}
