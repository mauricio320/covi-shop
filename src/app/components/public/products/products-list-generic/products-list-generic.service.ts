import { Injectable } from "@angular/core";
import { AngularFirestore } from "@angular/fire/firestore";

@Injectable({
  providedIn: "root",
})
export class ProductsListGenericService {
  constructor(private firestore: AngularFirestore) {}

  getProductos() {
    return this.firestore.collection("productos").snapshotChanges();
  }
}
