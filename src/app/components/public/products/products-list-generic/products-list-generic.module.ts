import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { ProductsListGenericComponent } from "./products-list-generic.component";
import { Routes, RouterModule } from "@angular/router";
import { ProductsListModule } from "../products-list/products-list.module";

const routes: Routes = [
  { path: "", component: ProductsListGenericComponent },
];

@NgModule({
  imports: [CommonModule, RouterModule.forChild(routes), ProductsListModule],
  declarations: [ProductsListGenericComponent],
})
export class ProductsListGenericModule {}
