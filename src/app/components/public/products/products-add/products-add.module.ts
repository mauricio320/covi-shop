import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { ProductsAddComponent } from "./products-add.component";
import { Routes, RouterModule } from "@angular/router";
import { QuillModule } from "ngx-quill";
import { FormsModule } from "@angular/forms";
import { DropdownModule } from "primeng/dropdown";
import { ToastModule } from "primeng/toast";

const routes: Routes = [{ path: "", component: ProductsAddComponent }];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    QuillModule.forRoot(),
    FormsModule,
    DropdownModule,
    ToastModule,
  ],
  declarations: [ProductsAddComponent],
})
export class ProductsAddModule {}
