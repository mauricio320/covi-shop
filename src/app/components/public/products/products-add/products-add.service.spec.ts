/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { ProductsAddService } from './products-add.service';

describe('Service: ProductsAdd', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ProductsAddService]
    });
  });

  it('should ...', inject([ProductsAddService], (service: ProductsAddService) => {
    expect(service).toBeTruthy();
  }));
});
