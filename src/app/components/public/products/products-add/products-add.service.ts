import { Injectable } from "@angular/core";
import { AngularFirestore } from "@angular/fire/firestore";

@Injectable({
  providedIn: "root",
})
export class ProductsAddService {
  constructor( private firestore: AngularFirestore) {

	}

	saveProducto(data: any) {
		return this.firestore.collection("productos").add(data)
	}
}
