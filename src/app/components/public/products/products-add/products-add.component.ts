import { Component, OnInit } from "@angular/core";
import { AuthRegisterStoreService } from "../../auth/auth-register-store/auth-register-store.service";
import { Router } from "@angular/router";
import { MessageService } from "primeng/api";
import { BlockingService } from "app/components/shared/blocking/blocking.service";
import { ProductsAddService } from "./products-add.service";
import { IsLoginService } from "app/components/shared/is-loggin/is-login.service";

@Component({
  selector: "app-products-add",
  templateUrl: "./products-add.component.html",
  styleUrls: ["./products-add.component.scss"],
  providers: [MessageService],
})
export class ProductsAddComponent implements OnInit {
  public categorias: any[];
  public modules = {
    toolbar: [
      [{ header: [1, 2, false] }],
      ["bold", "italic", "underline"],
      ["image"],
    ],
  };

  public auth: any = {};
  public empresa: any = {};
  public inputType: string = "password";
  public file: any;
  public imgPerfil: any =
    "https://images.immediate.co.uk/volatile/sites/3/2017/11/imagenotavailable1-39de324.png?quality=90&resize=768,574";

  constructor(
    private authRegisterStoreService: AuthRegisterStoreService,
    private blockingService: BlockingService,
    private messageService: MessageService,
    private productsAddService: ProductsAddService,
    private isLoginService: IsLoginService,
    public router: Router
  ) {
    this.categorias = [];
  }

  ngOnInit() {}

  viewImgTemp(file: any) {
    this.file = file;
    var _file: File = file.target.files[0];
    var myReader: FileReader = new FileReader();

    myReader.onloadend = (e) => {
      this.imgPerfil = myReader.result;
    };
    myReader.readAsDataURL(_file);
  }

  async saveImgAssets(file: any) {
    await this.authRegisterStoreService.tareaCloudStorage(file).then((res) => {
      this.empresa["img"] = res.metadata.fullPath;
    });
  }

  async saveData() {
    this.blockingService.open();
    await this.saveImgAssets(this.file);
    this.empresa["empresa_id"] = this.isLoginService.isLogged.id;
    this.productsAddService.saveProducto(this.empresa).then((res) => {
      this.validUserEmail();
    });
  }

  validUserEmail() {
    this.messageService.add({
      severity: "success",
      summary: "felicitaciones",
			detail: "Su producto se ha guardado exitosamente!",
    });

    setTimeout(() => {
      this.blockingService.close();
      this.router.navigate([
        `/store/detail/${this.isLoginService.isLogged.id}/${this.isLoginService.isLogged.id}`,
      ]);
    }, 2000);
  }
}
