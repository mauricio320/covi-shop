import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { Routes, RouterModule } from "@angular/router";
const store: Routes = [
  {
    path: "list",
    loadChildren: () =>
      import("./products-list-generic/products-list-generic.module").then(
        (m) => m.ProductsListGenericModule
      ),
  },
  {
    path: "detail",
    loadChildren: () =>
      import("./products-detail/products-detail.module").then(
        (m) => m.ProductsDetailModule
      ),
  },
  {
    path: "add",
    loadChildren: () =>
      import("./products-add/products-add.module").then(
        (m) => m.ProductsAddModule
      ),
  },
];

@NgModule({
  imports: [CommonModule, RouterModule.forChild(store)],
  declarations: [],
})
export class ProductsModule {}
