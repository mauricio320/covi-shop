import { Component, OnInit } from "@angular/core";
import { IsLoginService } from "../shared/is-loggin/is-login.service";

declare const $: any;
declare interface RouteInfo {
  path: string;
  title: string;
  icon: string;
  class: string;
}
export var ROUTES: RouteInfo[] = [];

export const origin: RouteInfo[] = [
  {
    path: "/store/list",
    title: "Tiendas",
    icon: "store_mall_directory",
    class: "",
  },
  {
    path: "/products/list",
    title: "Productos",
    icon: "shopping_cart",
    class: "",
  },
];

@Component({
  selector: "app-sidebar",
  templateUrl: "./sidebar.component.html",
  styleUrls: ["./sidebar.component.css"],
})
export class SidebarComponent implements OnInit {
  menuItems: any[];

  constructor(public isLoginService: IsLoginService) {}

  ngOnInit() {
    this.setMenu();
    this.isLoginService.SubjectLogin().subscribe((res) => {
      setTimeout(() => {
        this.setMenu();
      }, 100);
    });
  }

  setMenu() {
    ROUTES = [];

    if (this.isLoginService.isLogged.tipo === 1) {
      ROUTES.push({
        path: "/profile/store",
        title: "Perfil",
        icon: "person",
        class: "",
      });
    }
    if (this.isLoginService.isLogged.tipo === 2) {
      ROUTES.push({
        path: "/profile/user",
        title: "Perfil",
        icon: "person",
        class: "",
      });
    }

    ROUTES = [...ROUTES, ...origin];
    if (this.isLoginService.isLogged.tipo === 1) {
      ROUTES.push({
        path: "/products/add",
        title: "Publicar",
        icon: "add_shopping_cart",
        class: "",
      });
      ROUTES.push({
        path: "/chat/store",
        title: "Chats",
        icon: "mail_outline",
        class: "",
      });
    }

    if (this.isLoginService.isLogged.tipo === 2) {
      ROUTES.push({
        path: "/chat/user",
        title: "Chats",
        icon: "mail_outline",
        class: "",
      });
    }

    if (this.isLoginService.isLogged.loggin) {
      ROUTES.push({
        path: "/auth/isLogin",
        title: "cerrar sesion",
        icon: "arrow_right_alt",
        class: "",
      });
    }

    this.menuItems = ROUTES.filter((menuItem) => menuItem);
  }
  isMobileMenu() {
    if ($(window).width() > 991) {
      return false;
    }
    return true;
  }
}
