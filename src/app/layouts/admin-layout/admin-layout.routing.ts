import { Routes } from "@angular/router";

export const AdminLayoutRoutes: Routes = [
  {
    path: "store",
    loadChildren: () =>
      import("../../components/public/store/store.module").then(
        (m) => m.StoreModule
      ),
  },
  {
    path: "products",
    loadChildren: () =>
      import("../../components/public/products/products.module").then(
        (m) => m.ProductsModule
      ),
  },
  {
    path: "auth",
    loadChildren: () =>
      import("../../components/public/auth/auth.module").then(
        (m) => m.AuthModule
      ),
  },
  {
    path: "chat",
    loadChildren: () =>
      import("../../components/public/chats/chats.module").then(
        (m) => m.ChatsModule
      ),
  },
  {
    path: "profile",
    loadChildren: () =>
      import("../../components/public/profile/profile.module").then(
        (m) => m.ProfileModule
      ),
  },
];
