// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyDAL1elclVM89trdeuYadXV-rcB8sQ14w8",
    authDomain: "covi-store.firebaseapp.com",
    databaseURL: "https://covi-store.firebaseio.com",
    projectId: "covi-store",
    storageBucket: "covi-store.appspot.com",
    messagingSenderId: "91677334165",
    appId: "1:91677334165:web:a544b7d3527beedd30599c",
  },
};
